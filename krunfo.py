from csv import DictReader, DictWriter
import random


def list_all_super_heroes(filename):
    with open(filename) as f:
        reader = DictReader(f)
        data = list(reader)

        return data


def get_players_decks(card_list: list):
    len_card_list = len(card_list) 
    if len(card_list) % 2 != 0:
        
        return None

    random.shuffle(card_list)
    player_a_cards = card_list[:(len_card_list // 2)]
    player_b_cards = card_list[(len_card_list // 2):]
    
    return (player_a_cards, player_b_cards)


def compare_cards(player_a_card, player_b_card, score):
    attribute_list = ['intelligence', 'power', 'strength', 'agility', 'vitality']
    r = random.randint(0, 4)

    if player_a_card[attribute_list[r]] > player_b_card[attribute_list[r]]:
        score[0] += 1 
    if player_b_card[attribute_list[r]] > player_a_card[attribute_list[r]]:
        score[1] += 1 

    return score


def play(card_list, score):
    player_a_cards, player_b_cards = get_players_decks('super_heroes.csv')
    
    return score    

print(play())
